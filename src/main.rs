#![no_std]
#![no_main]

use panic_halt as _;

#[link_section = ".boot2"]
#[used]
pub static BOOT2: [u8; 256] = rp2040_boot2::BOOT_LOADER_W25Q080;

#[rtic::app(device = pro_micro_rp2040::hal::pac, peripherals = true, dispatchers = [SPI0_IRQ, SPI1_IRQ])]
mod app {
    use pro_micro_rp2040::{
        hal::{
            usb::UsbBus,
            //prelude::*,
            clocks::init_clocks_and_plls,
            clocks::Clock,
            pac,
            //sio::Sio,
            watchdog::Watchdog,
        },
        XOSC_CRYSTAL_FREQ,
    };
    use systick_monotonic::Systick;
    use usbd_hid::descriptor::generator_prelude::*;
    use usbd_hid::descriptor::MouseReport;
    use usbd_hid::hid_class::HIDClass;
    use usb_device::bus::UsbBusAllocator;
    use usb_device::prelude::*;
    use rtic::time::duration::*;
    use embedded_time::rate::Hertz;

    #[shared]
    struct Shared {
        usb_hid: HIDClass<'static, UsbBus>,
    }

    #[local]
    struct Local {
        direction: bool,
        usb_dev: UsbDevice<'static, UsbBus>,
    }

    #[monotonic(binds = SysTick, default = true)]
    type MyMono = Systick<10_000>;

    #[init(local = [
        usb_bus: Option<UsbBusAllocator<UsbBus>> = None
    ])]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        let usb_bus: &'static mut Option<UsbBusAllocator<UsbBus>> = cx.local.usb_bus;
        let mut resets = cx.device.RESETS;
        let mut watchdog = Watchdog::new(cx.device.WATCHDOG);
        let clocks = init_clocks_and_plls(
            XOSC_CRYSTAL_FREQ,
            cx.device.XOSC,
            cx.device.CLOCKS,
            cx.device.PLL_SYS,
            cx.device.PLL_USB,
            &mut resets,
            &mut watchdog
        ).ok().unwrap();

        let mono = Systick::new(cx.core.SYST, clocks.system_clock.freq().0);

        usb_bus.replace(UsbBusAllocator::new(UsbBus::new(
            cx.device.USBCTRL_REGS,
            cx.device.USBCTRL_DPRAM,
            clocks.usb_clock,
            true,
            &mut resets,
        )));

        let usb_hid = HIDClass::new(usb_bus.as_ref().unwrap(), MouseReport::desc(), 60);

        let usb_dev = UsbDeviceBuilder::new(usb_bus.as_ref().unwrap(), UsbVidPid(0xc410, 0x0000))
            .manufacturer("Wiggle Tech")
            .product("Mouse")
            .serial_number("CAT")
            .device_class(0) // class misc
            .build();

        twitch::spawn_after(5.seconds()).ok();

        usb_handler::spawn_after(1.milliseconds()).ok();

        (
            Shared {
                usb_hid,
            },
            Local {
                direction: false,
                usb_dev,
            },
            init::Monotonics(mono),
        )
    }

    #[task(priority=2, local=[direction], shared=[usb_hid])]
    fn twitch(mut cx: twitch::Context) {
        let direction = cx.local.direction;
        cx.shared.usb_hid.lock(|hid| {
            let input = MouseReport {
                x: if *direction { 4 } else { -4 } ,
                y: 0,
                buttons: 0,
                wheel: 0,
                pan: 0,
            };
            *direction = !*direction;
            let _ = hid.push_input(&input).ok().unwrap_or(0);
        });
        twitch::spawn_after(5.seconds()).ok();
    }

    #[task(priority=1, local=[usb_dev], shared=[usb_hid])]
    fn usb_handler(mut cx: usb_handler::Context) {
        let usb_dev = cx.local.usb_dev;
        cx.shared.usb_hid.lock(|hid| {
            usb_dev.poll(&mut [hid]);
        });
        usb_handler::spawn_after(1.milliseconds()).ok();
    }


}
